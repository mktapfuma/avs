const express = require('express')
const app = express()
const port = 81

var chickens = [
    "robo-chicken",
    "space-chicken",
    "commando-chicken",
    "pirate-chicken"
]

app.get('/api/chickens', function (req, res) {
    res.send(chickens)
})

app.post('/api/chickens/:name', function (req, res) {
  chickens.push(req.params.name)
  res.send(chickens)
})

app.delete('/api/chickens/:name', function(req, res) {
    var index = chickens.indexOf(req.params.name)

    if (index > -1) {
        chickens.splice(index, 1)
    }

    res.send(chickens)
})

app.listen(port, () => console.log(`Listening on port ${port}`))